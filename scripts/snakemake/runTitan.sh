#!/bin/bash

# get the reference and liftover files from bucket
# mkdir /data/
# echo "Make /data/ directory done!"
# gsutil cp -r gs://bucket1q2w/jpliu/titancna/tcga/refgrch38/cytoBand_hg38.txt /data/
# echo "Copy ref data done!"
# gsutil cp -r gs://bucket1q2w/jpliu/titancna/tcga/liftover /data/
# echo "Copy liftover data done!"
####
## Mount a disk on existing container direcotry will miss all the files in container.
### Here I copy them to the mounted disk for process.
cp -r /home/usr/* /mnt/data
cd /mnt/data/titancna/scripts/snakemake/
snakemake -s TitanCNA.snakefile -j 4 -p --config bamMeta=$bamMeta \
samplesList=$samplesList \
pairList=$pairList \
run=$run

# The previous setting by copy to $OUTPUT_DIR works
# It will run sync when the full script gets executed.
if [ -f ${run}.titanca.done ]; then
	gsutil -m cp -r results/titan/hmm/* gs://bucket1q2w/phylowgs_mc3/non_pan12/titancna_res/
	gsutil cp results/titan/tumCounts/${run}.tumCounts.txt gs://bucket1q2w/phylowgs_mc3/non_pan12/tumCounts/
	gsutil cp results/ichorCNA/${run}/${run}.correctedDepth.txt gs://bucket1q2w/phylowgs_mc3/non_pan12/ichorCNA/
	#cp -r logs/* $OUTPUT_DIR
	gsutil cp ${run}.titanca.done gs://bucket1q2w/phylowgs_mc3/non_pan12/titancna_jobs/
fi
