configfile: "config/config.yaml"
configfile: config['samplesList']
configfile: config['pairList']

# def loadBamManifest(meta):
#     dic = dict()
#     with open(meta,'r') as fi:
#         for line in fi:
#             fs = line.strip().split()
#             bc = fs[6]
#             uuid = fs[0]
#             dic[bc] = uuid
#     return dic
# config['barcode2uuid'] = loadBamManifest(config['bamMeta'])


def loadISBCGCMeta(meta):
    dic = dict()
    with open(meta,'r') as fi:
        next(fi)
        for line in fi:
            fs = line.strip().split('\t')
            bc = fs[2]
            fid = fs[13]
            dic[bc] = fid
    return dic
config['barcode2gs'] = loadISBCGCMeta(config['bamMeta'])
### Using GDC tools for data downloading
# rule download_bam:
#     output:
#         'data/gdc/{sample}.done'
#     params:
#          uuid = lambda wildcards: config['barcode2uuid'][wildcards.sample]
#     threads:
#         4
#     shell:
#     # It will timeout after 15m maximum run for gdc-client
#     # if more than 15m, stop at gdc-client and will not touch
#         "timeout 5h gdc-client download -t {config[token]} "
#         "-d data/ -n {threads} {params.uuid} "
#         "&& touch {output} && bash fixindex.sh data/{params.uuid}"


### Using ISB-CGC to download bam as well as bai


rule download_bam:
    output:
        'data/gdc/{sample}.done'
    params:
         gsfile = lambda wildcards: config['barcode2gs'][wildcards.sample],
         local = lambda wildcards: config['samples'][wildcards.sample]
    shell:
        "gsutil cp $(echo {params.gsfile} | sed 's/bam$/bai/') data/ && "
        "mv $(echo {params.local} | sed 's/bam$/bai/') {params.local}.bai && "
        "gsutil cp {params.gsfile} data/ && "
        "touch {output}"
